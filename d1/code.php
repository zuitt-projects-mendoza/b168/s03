<?php

// Objects as Variables

$buildingObj = (object)[
	'name' => 'Caswynn Building',
	'floor' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

// Objects from Classess

class Building {
	public $name;
	public $floor;
	public $address;

	// Constructor is used during the creation of an object.

	public function __construct($name, $floor, $address){
		$this->name = $name;
		$this->floor = $floor;
		$this->address = $address;
	}

	public function printName(){
		return "The name of the building is $this->name";
	}
}

class Condominium extends Building{
	public function printName(){
		return "The name of the condominium is $this->name";
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');